<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 16/06/15
 * Time: 14:36
 */

use Core\Settings;
use Core\Database\MysqlDatabase;

class App {

    public $title = 'Blog';

    private $db_instance;

    private static $_instance;

    public static function getInstance(){
        if(is_null(self::$_instance)){
            self::$_instance = new App();
        }

        return self::$_instance;
    }

    public function getTable($name){
        $class_name = '\\App\\Tables\\'. ucfirst($name) .'Table';

        return new $class_name($this->getDb());
    }

    public static function load(){
        session_start();

        require ROOT.'/app/Autoloader.php';
        \App\Autoloader::register();

        require ROOT.'/core/Autoloader.php';
        \Core\Autoloader::register();
    }

    public function getDb(){
        $config = Settings::getInstance(ROOT. '/config/Main.php');

        if(is_null($this->db_instance)){
            $this->db_instance = new MysqlDatabase($config->get('db_name'), $config->get('db_user'), $config->get('db_password'), $config->get('db_host'));
        }

        return $this->db_instance;

    }




}