<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 18/06/15
 * Time: 17:17
 */

namespace App\Controller\Admin;


class AppController {

    protected $template = 'default';

    public function __construct(){

        $this->viewPath = ROOT .'/app/View/';
    }

    public function loadModel($model_name){
        $this->$model_name = \App::getInstance()->getTable($model_name);
    }

}