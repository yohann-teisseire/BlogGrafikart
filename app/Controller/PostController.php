<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 18/06/15
 * Time: 15:51
 */

namespace App\Controller;


use App\Controller\AppController;

class PostController extends AppController{

    public function __construct(){
        parent::__construct();
        $this->loadModel('article');
        $this->loadModel('categorie');
    }

    public function index(){
        $posts = $this->article->last();

        $categories = $this->categorie->all();

        $this->render('posts.index', compact('posts', 'categories'));
    }

    public function categories(){

        $app = App::getInstance();
        $categorie = $this->categorie->find([$_GET['id']]);

        if($categorie === false){
            $this->notFound();
        }

        $articles = $this->article->lastByCategorie($_GET['id']);

        $categories = $this->categorie->all();

        $this->render('posts.categorie', compact('categorie', 'categories', 'articles'));

    }

    public function show(){
        $article = $this->article->findWithCategory($_GET['id']);

        $this->render('posts.show', compact('article'));
    }

}