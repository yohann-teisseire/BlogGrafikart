<?php

namespace App\Tables;

use Core\Table\Table;

Class ArticleTable extends Table{


    protected $table = 'articles';

    /**
     * @return mixed
     */
    public function last(){
        return $this->query('
            SELECT id_article, articles.titre, articles.contenu, articles.date , categories.titre as categorie
            FROM articles
            LEFT JOIN categories ON id_categorie = categories.categorie_id
            ORDER BY articles.date DESC ');
    }

    public function find($id){
        return $this->query('
            SELECT id_article, articles.titre, articles.contenu, articles.date , categories.titre as categorie
            FROM articles
            LEFT JOIN categories ON id_categorie = categories.categorie_id
            WHERE id_article = ?', [$id], true);
    }

    public function lastByCategorie($categorie_id){
        return $this->query('
            SELECT id_article, articles.titre, articles.contenu, articles.date , categories.titre as categorie
            FROM articles
            LEFT JOIN categories ON id_categorie = categories.categorie_id
            WHERE articles.id_article = ?
            ORDER BY articles.titre DESC ', [$categorie_id]);
    }
}