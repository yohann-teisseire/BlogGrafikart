<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 16/06/15
 * Time: 16:25
 */

namespace App\Tables;


use App\App;
use Core\Table\Table;

class CategorieTable extends Table{

    protected $table = 'categories';

}