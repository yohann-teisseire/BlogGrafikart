<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 18/06/15
 * Time: 15:50
 */

namespace Core\Controller;


class Controller {

    protected $viewPath;
    protected $template;

    protected function render($view, $variables = []){
        ob_start();
        extract($variables);
        require ($this->viewPath . str_replace('.' , '/', $view). '.php');

        $content = ob_get_clean();

        require ($this->viewPath . 'templates/' . $this->template . '.php');
    }

    protected function forbidden(){
        header('HTTP:/1.0 403 Forbidden');
        die('Accès interdit');
    }

    protected function notFound(){
        header('HTTP:/1.0 404 Not Found');
        die('Page Introuvable');
    }



}