<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 17/06/15
 * Time: 10:48
 */

namespace Core;


class Settings {

    private $settings = [];

    private static $_instance;

    public static function getInstance($file){
        if(is_null(self::$_instance)) {
            self::$_instance = new Settings($file);
        }

        return self::$_instance;

    }

    public function __construct($file){
        $this->settings = require($file);
    }

    public function get($key){
        if(!isset($this->settings[$key])){
            return null;
        }

        return $this->settings[$key];
    }

}