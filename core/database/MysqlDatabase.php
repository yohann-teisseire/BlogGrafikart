<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 17/06/15
 * Time: 15:04
 */

namespace Core\Database;
use \PDO;

class MysqlDatabase extends Database{


    public $db_name = 'blogGrafikart';
    private $db_user = 'root';
    private $db_pass = 'root';
    private $db_host = 'localhost';
    private $pdo;

    public function __construc($db_name, $db_user , $db_pass, $db_host){
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_pass = $db_pass;
        $this->db_host = $db_host;
    }


    private function getPDO(){

        if($this->pdo === null){ //evite la fuite de mémoire, PDO est chargé qu'une fois
            $pdo = new PDO('mysql:dbname=blogGrafikart;host=localhost','root','root');

            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->pdo = $pdo;
        }

        return $this->pdo;
    }

    public function query($statement, $class_name, $one = false){
        $req = $this->getPDO()->query($statement);

        $req->setFetchMode(PDO::FETCH_CLASS, $class_name);

        if ($one) {

            $datas = $req->fetch();

        } else {

            $datas = $req->fetchAll();

        }

        return $datas;
    }

    public function prepare($statement, $attributes, $class_name, $one = false){
        $req = $this->getPDO()->prepare($statement);

        if( strpos($statement, 'UPDATE') === 0 || strpos($statement, 'INSERT') === 0 || strpos($statement, 'DELETE') === 0){
           return $req->execute($attributes);
        }


        $req->setFetchMode(PDO::FETCH_CLASS, $class_name);

        if ($one) {

            $datas = $req->fetch();

        } else {

            $datas = $req->fetchAll();

        }



        return $datas;
    }

    public function lastInsertId(){
        return $this->getPDO()->lastInsertId();
    }
}
