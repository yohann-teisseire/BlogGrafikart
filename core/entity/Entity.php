<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 17/06/15
 * Time: 16:34
 */

namespace Core\Entity;


class Entity {

    public function __get($key){
        $method = 'get' . ucfirst($key);

        $this->$key = $this->$method();

        return $this->$key;
    }

}