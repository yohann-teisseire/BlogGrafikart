<?php
/**
 * Created by PhpStorm.
 * User: macmce
 * Date: 17/06/15
 * Time: 15:06
 */

namespace Core\Table;
use App\App;
use Core\Database\Database;

class Table {


    protected $table;

    protected $db;

    public function __construct(Database $db){
        $this->db = $db;

        if(is_null($this->table)){
            $parts = explode('\\', get_class($this));
            $class_name = end($parts);
            $this->table = strtolower(str_replace('Table', '', $class_name)) .'s';
        }
    }


    public function all(){
        return $this->query('SELECT * FROM ' . $this->table);
    }

    public function query($statement, $attrs = null, $one = false){
        if($attrs){
             return  $this->db->prepare(
                $statement,
                $attrs,
                str_replace('Table', 'Entity' , get_class($this)),
                $one);
        }else{
            return $this->db->query(
                $statement,
                str_replace('Table', 'Entity' , get_class($this)),
                $one);
        }
    }

    public function find($id){
        return $this->query("SELECT * FROM  {$this->table} WHERE id = ?", [$id], true);
    }

}