<?php

define ('ROOT', dirname(__DIR__));
use App\Controller\PostController;

require ROOT .'/app/App.php';

App::load();

if(isset($_GET['p'])){
    $page = $_GET['p'];
}else{
    $page = 'home';
}

if($page === 'home'){
    $controller = new PostController();
    $controller->index();

}   elseif ($page === 'posts.category'){

        $controller = new PostController();
        $controller->categories();

}   elseif ($page === 'posts.show') {

        $controller = new PostController();
        $controller->show();
}
    elseif ($page === 'login') {

        $controller = new UserController();
        $controller->login();
}













